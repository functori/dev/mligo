open Mligo
include Types
include Fa2 [@@path "../utils/fa2.mligo"]
module Admin = Admin [@@path "../utils/admin.mligo"]

type mint_info =
  | New of (string, bytes) map
  | Add of nat

type mint = {
  mi_owners: (address, int) map;
  mi_info: mint_info;
}

let%entry mint (m : mint)  (s : storage) : operation list * storage =
  let () = Admin.fail_if_not_admin s.admin in
  let id, exists, tot, next = match m.mi_info with
    | New _ -> s.next, false, 0n, s.next + 1n
    | Add id ->
      match Big_map.find_opt id s.supply with
      | None -> failwith fa2_token_undefined
      | Some tot -> id, true, tot, s.next in
  let tot, ledger =
    Map.fold (fun ((tot, ledger), (owner, am) : (nat * ledger) * (address * int)) ->
        match is_nat am with
        | Some am -> (* Mint *)
          if am = 0n then tot, ledger
          else
            let tot = tot + am in
            if not exists then tot, Big_map.update (owner, id) (Some am) ledger
            else begin match Big_map.find_opt (owner, id) ledger with
              | None -> tot, Big_map.update (owner, id) (Some am) ledger
              | Some am0 -> tot, Big_map.update (owner, id) (Some (am0 + am)) ledger
            end
        | None -> (* Burn *)
          if not exists then failwith fa2_token_undefined
          else match Big_map.find_opt (owner, id) ledger with
            | None -> failwith fa2_not_owner
            | Some am0 ->
              match is_nat (int am0 + am), is_nat (int tot + am) with
              | Some am, Some tot ->
                let ledger =
                  if am = 0n then Big_map.remove (owner, id) ledger
                  else Big_map.update (owner, id) (Some am) ledger in
                tot, ledger
              | _ -> failwith fa2_insufficient_balance)
      m.mi_owners (tot, s.ledger) in
  let supply = Big_map.update id (Some tot) s.supply in
  let s = { s with next; ledger; supply } in
  match m.mi_info with
  | Add _ -> [], s
  | New m ->
    let token_metadata = Big_map.update id (Some (id, m)) s.token_metadata in
    [], { s with token_metadata }
