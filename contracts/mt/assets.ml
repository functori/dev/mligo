open Mligo
include Types
include Fa2 [@@path "../utils/fa2.mligo"]
module Admin = Admin [@@path "../utils/admin.mligo"]

let%inline balance (owner: address) (token_id: nat) (s: storage) : nat =
  if token_id >= s.next then failwith fa2_token_undefined
  else match Big_map.find_opt (owner, token_id) s.ledger with
    | None -> 0n
    | Some am -> am

let%view get_balance (owner, token_id: address * nat) (s: storage) : nat =
  balance owner token_id s

let%entry balance_of (p: balance_of_param) (s: storage) : operation list * storage =
  let l = List.map (fun (ba_request : balance_of_request) ->
    let ba_balance = balance ba_request.ba_owner ba_request.ba_token_id s in
    { ba_request; ba_balance }) p.ba_requests in
  [ Operation.transaction None l 0u p.ba_callback ], s

let%entry transfer (txs: transfer list) (s: storage) : operation list * storage =
  let sender = Next.get_sender None in
  let ledger = List.fold (fun (l, tx : ledger * transfer) ->
    List.fold (fun (ll, dst : ledger * transfer_destination) ->
      if dst.tr_token_id >= s.next then failwith fa2_token_undefined
      else match Big_map.find_opt (tx.tr_src, dst.tr_token_id) ll with
        | None -> failwith fa2_insufficient_balance
        | Some am ->
          if dst.tr_amount = 0n then ll
          else match is_nat (am - dst.tr_amount) with
            | None -> failwith fa2_insufficient_balance
            | Some diff ->
              let () =
                  if tx.tr_src = sender then ()
                  else if Big_map.mem (tx.tr_src, (sender, dst.tr_token_id)) s.operators then ()
                  else failwith fa2_not_operator in
              let ll =
                if diff = 0n then Big_map.remove (tx.tr_src, dst.tr_token_id) ll
                else Big_map.update (tx.tr_src, dst.tr_token_id) (Some diff) ll in
              match Big_map.find_opt (dst.tr_dst, dst.tr_token_id) ll with
              | None -> Big_map.add (dst.tr_dst, dst.tr_token_id) dst.tr_amount ll
              | Some am -> Big_map.update (dst.tr_dst, dst.tr_token_id) (Some (am + dst.tr_amount)) ll)
      tx.tr_txs l)
    txs s.ledger in
  [], { s with ledger }

let%entry update_operators (ops: operator_update list) (s: storage) : operation list * storage =
  let sender = Next.get_sender None in
  let storage = List.fold (fun (storage, update : storage * operator_update) ->
    let u, v = match update with
      | Add_operator update -> update, Some ()
      | Remove_operator update -> update, None in
    if u.op_owner <> sender then failwith fa2_not_owner
    else if u.op_token_id >= s.next then failwith fa2_token_undefined
    else
      let operators = Big_map.update (u.op_owner, (u.op_operator, u.op_token_id)) v s.operators in
      { storage with operators }
  ) ops s in
  [], storage

let%view total_supply (token_id: nat) (s: storage): nat =
  match Big_map.find_opt token_id s.supply with
  | None -> failwith fa2_token_undefined
  | Some s -> s
