open Mligo
include Types
include Fa2 [@@path "../utils/fa2.mligo"]
module Admin = Admin [@@path "../utils/admin.mligo"]

type mint = (address * nat) list

let%entry mint (m: mint) (s: storage) : operation list * storage =
  let () = Admin.fail_if_not_admin s.admin in
  let ledger, supply = List.fold (fun ((ledger, supply), (owner, am): (ledger * nat) * (address * nat)) ->
    let a = match Big_map.find_opt owner ledger with
      | Some a -> a + am
      | None -> am in
    Big_map.update owner (Some a) ledger, supply + a)
    m (s.ledger, s.supply) in
  [], { s with ledger; supply }

let%entry burn (a: nat) (s: storage) : operation list * storage =
  match Big_map.find_opt (Next.get_sender None) s.ledger with
  | None -> failwith fa2_not_owner
  | Some am -> match is_nat (am - a), is_nat (s.supply - a) with
    | Some am2, Some supply ->
      let ledger = Big_map.update (Next.get_sender None) (Some am2) s.ledger in
      [], { s with ledger; supply }
    | _ -> failwith fa2_insufficient_balance
