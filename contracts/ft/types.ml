open Mligo
module Admin = Admin [@@path "../utils/admin.mligo"]
module Fa2 = Fa2 [@@path "../utils/fa2.mligo"]

type ledger = (address, nat) big_map
type operators = (address * address, unit) big_map

type storage = {
  admin: Admin.storage;
  ledger: ledger;
  operators: operators;
  token_metadata: Fa2.token_metadata;
  supply: nat;
  metadata: (string, bytes) big_map;
}
