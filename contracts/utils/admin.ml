open Mligo

type storage = { current: address; pending: address option; paused: bool }

let%inline fail_if_not_admin (a : storage) : unit =
  if Tezos.Next.get_sender None <> a.current then failwith "NOT_AN_ADMIN" else ()

let%inline fail_if_paused (a : storage) : unit =
  if a.paused then failwith "PAUSED" else ()

let set_admin (a: address) (s: storage) : storage =
  let () = fail_if_not_admin s in
  { s with pending = Some a }

let confirm_admin (s: storage) : storage =
  match s.pending with
  | None -> (failwith "NO_PENDING_ADMIN" : storage)
  | Some pending ->
    let current = Tezos.Next.get_sender None in
    if current = pending then
      { s with pending = (None : address option); current }
    else (failwith "NOT_A_PENDING_ADMIN" : storage)

let pause (paused : bool) (s : storage) : storage =
  let () = fail_if_not_admin s in
  { s with paused }
