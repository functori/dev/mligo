open Mligo

type transfer_destination = {
  tr_dst : address; [@key "to_"]
  tr_token_id : nat;
  tr_amount : nat;
}

type transfer = {
  tr_src : address; [@key "from_"]
  tr_txs : transfer_destination list;
}

type operator_param = {
  op_owner : address;
  op_operator : address;
  op_token_id: nat;
}

type operator_update =
  | Add_operator of operator_param
  | Remove_operator of operator_param

type balance_of_request = {
  ba_owner : address;
  ba_token_id : nat;
}

type balance_of_response = {
  ba_request : balance_of_request;
  ba_balance : nat;
}

type balance_of_param = {
  ba_requests : balance_of_request list;
  ba_callback : (balance_of_response list, unit) contract;
}

type token_metadata = (nat, nat * (string, bytes) map) big_map

let%inline fa2_token_undefined = "FA2_TOKEN_UNDEFINED"
let%inline fa2_insufficient_balance = "FA2_INSUFFICIENT_BALANCE"
let%inline fa2_tx_denied = "FA2_TX_DENIED"
let%inline fa2_not_owner = "FA2_NOT_OWNER"
let%inline fa2_not_operator = "FA2_NOT_OPERATOR"
