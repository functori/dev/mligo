open Mligo
module Admin = Admin [@@path "../utils/admin.mligo"]
module Fa2 = Fa2 [@@path "../utils/fa2.mligo"]

type mint_param = (address * (string, bytes) map list) list
type burn_param = nat list

type ledger = (nat, address) big_map
type operators = ((address * (address * nat)), unit) big_map

type storage = {
  admin : Admin.storage;
  ledger : ledger;
  operators : operators;
  token_metadata : Fa2.token_metadata;
  next : nat;
  metadata : (string, bytes) big_map;
}
