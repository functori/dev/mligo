open Mligo
include Types
include Fa2 [@@path "../utils/fa2.mligo"]
module Admin = Admin [@@path "../utils/admin.mligo"]

let%inline balance (owner: address) (token_id: nat) (s: storage) : nat =
  match Big_map.find_opt token_id s.ledger with
  | None -> failwith fa2_token_undefined
  | Some addr -> if owner = addr then 1n else 0n

let%view get_balance (owner, token_id: address * nat) (s: storage) : nat =
  balance owner token_id s

let%entry balance_of (p : balance_of_param) (s: storage) : operation list * storage =
  let to_balance (ba_request : balance_of_request) =
    let ba_balance = balance ba_request.ba_owner ba_request.ba_token_id s in
    { ba_request; ba_balance } in
  let responses = List.map to_balance p.ba_requests in
  let op = Operation.transaction None responses 0u p.ba_callback in
  [ op ], s

let%entry transfer (txs: transfer list) (s: storage) : operation list * storage =
  let () = Admin.fail_if_paused s.admin in
  let sender = Next.get_sender None in
  let ledger = List.fold (fun (ledger, tx : ledger * transfer) ->
    List.fold (fun (ledger, dst : ledger * transfer_destination) ->
      match Big_map.find_opt dst.tr_token_id ledger with
      | None -> (failwith fa2_token_undefined : ledger)
      | Some owner ->
        if owner <> tx.tr_src then (failwith fa2_not_owner : ledger)
        else
          let () =
            if owner = sender then ()
            else if Big_map.mem (owner, (sender, dst.tr_token_id)) s.operators then ()
            else (failwith fa2_not_operator : unit) in
          if dst.tr_amount = 0n then ledger
          else if dst.tr_amount > 1n then (failwith fa2_insufficient_balance : ledger)
          else Big_map.update dst.tr_token_id (Some dst.tr_dst) ledger)
      tx.tr_txs ledger)
    txs s.ledger in
  [], { s with ledger }

let%entry update_operators (ops : operator_update list) (s: storage) : operation list * storage =
  let () = Admin.fail_if_paused s.admin in
  let sender = Next.get_sender None in
  let operators = List.fold (fun (operators, update : operators * operator_update) ->
    let update, value = match update with
      | Add_operator update -> update, Some ()
      | Remove_operator update -> update, None in
    let () = if update.op_owner = sender then () else failwith fa2_not_owner in
    Big_map.update (update.op_owner, (update.op_operator, update.op_token_id)) value operators)
    ops s.operators in
  [], { s with operators }
