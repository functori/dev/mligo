open Mligo
include Types
include Fa2 [@@path "../utils/fa2.mligo"]
module Admin = Admin [@@path "../utils/admin.mligo"]

let%entry mint (p: mint_param) (s: storage) : operation list * storage =
  let () = Admin.fail_if_not_admin s.admin in
  let next, ledger, token_metadata =
    List.fold (fun ((acc, ledger, metadata), (owner, infos): ((nat * ledger * token_metadata) * (address * (string, bytes) map list))) ->
      List.fold (fun ((acc, ledger, metadata), info: (nat * ledger * token_metadata) * (string, bytes) map) ->
        let ledger = Big_map.add acc owner ledger in
        let metadata = Big_map.add acc (acc, info) metadata in
        (acc+1n, ledger, metadata))
        infos (acc, ledger, metadata))
      p (s.next, s.ledger, s.token_metadata) in
  [], { s with next; ledger; token_metadata }

let%entry burn (p: burn_param) (s: storage) : operation list * storage =
  let () = Admin.fail_if_not_admin s.admin in
  let ledger, token_metadata =
    List.fold (fun ((ledger, metadata), id: (ledger * token_metadata) * nat) ->
      Big_map.remove id ledger, Big_map.remove id metadata)
      p (s.ledger, s.token_metadata) in
  [], { s with ledger; token_metadata }
