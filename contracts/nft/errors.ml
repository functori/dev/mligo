(** One of the specified `token_id`s is not defined within the FA2 contract *)
let%inline fa2_token_undefined = "FA2_TOKEN_UNDEFINED"
(**
A token owner does not have sufficient balance to transfer tokens from
owner's account
*)
let%inline fa2_insufficient_balance = "FA2_INSUFFICIENT_BALANCE"
(**
A transfer failed because `operator_transfer_policy == Owner_transfer` and it is
initiated not by the token owner
*)
let%inline fa2_not_owner = "FA2_NOT_OWNER"
(**
A transfer failed because `operator_transfer_policy == Owner_or_operator_transfer`
and it is initiated neither by the token owner nor a permitted operator
 *)
let%inline fa2_not_operator = "FA2_NOT_OPERATOR"
