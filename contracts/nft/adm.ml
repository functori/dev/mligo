open Mligo
include Types
module Admin = Admin [@@path "../utils/admin.mligo"]

let%inline aux (admin: Admin.storage) (s: storage) : operation list * storage =
  [], { s with admin }

let%entry set_admin (a: address) (s: storage) : operation list * storage =
  aux (Admin.set_admin a s.admin) s

let%entry confirm_admin () (s: storage) : operation list * storage =
  aux (Admin.confirm_admin s.admin) s

let%entry pause (paused : bool) (s : storage) : operation list * storage =
  aux (Admin.pause paused s.admin) s
