open Std

val create : context option -> 'value -> nat -> 'value ticket option
val read : context option -> 'value ticket -> (address * ('value * nat)) * 'value ticket
val split : context option -> 'value ticket -> nat * nat -> ('value ticket * 'value ticket) option
val join : context option -> ('value ticket * 'value ticket) -> 'value ticket option
