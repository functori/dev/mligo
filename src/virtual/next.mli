open Std

module Operation = Operation
module View = View
module Ticket = Ticket
module Sapling = Sapling

val get_sender : context option -> address
val get_source : context option -> address
val self : context option -> string -> _ contract
val get_self_address : context option -> address (* to use at toplevel *)
val address : _ contract -> address
val implicit_account : context option -> key_hash -> (unit, unit) contract
val get_contract_opt : context option -> address -> _ contract option
val get_contract_with_error : context option -> address -> string -> _ contract
val get_contract : context option -> address -> _ contract
val get_entrypoint_opt : context option -> string -> address -> _ contract option
val get_entrypoint : context option -> string -> address -> _ contract
val open_chest : context option -> chest_key -> chest -> nat -> bytes option
val get_balance : context option -> tez
val get_amount : context option -> tez
val get_now : context option -> timestamp
val get_min_block_time : context option -> nat
val get_level : context option -> nat
val get_chain_id : context option -> chain_id
val get_total_voting_power : context option -> nat
val voting_power : context option -> key_hash -> nat
val never : never -> 'a
(* val pairing_check : (bls12_381_g1 * bls12_381_g2) list -> bool *)
val constant : context option -> string -> 'a
