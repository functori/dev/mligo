open Std

val and_nat : nat -> nat -> nat
val or_nat :  nat -> nat -> nat
val xor_nat :  nat -> nat -> nat
val shift_left_nat :  nat -> nat -> nat
val shift_right_nat :  nat -> nat -> nat

val and_bytes : string -> string -> string
val or_bytes :  string -> string -> string
val xor_bytes :  string -> string -> string
val shift_left_bytes :  string -> nat -> string
val shift_right_bytes :  string -> nat -> string
