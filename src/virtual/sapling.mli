open Std

val empty_state : context option -> 'n sapling_state
val verify_update : context option -> 'n sapling_transaction -> 'n sapling_state -> (int * 'n sapling_state) option
