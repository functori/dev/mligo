val unopt : 'a option -> 'a
val unopt_with_error : 'a option -> string -> 'a
val map : ('a -> 'b) -> 'a option -> 'b option
val value : 'a option -> 'a
val value_with_error : exn -> 'a option -> 'a
val is_none : 'a option -> bool
val is_some : 'a option -> bool
