module Big_map = Big_map
module Bitwise = Bitwise
module Bytes = Bytes
module Crypto = Crypto
module List = List
module Map = Map
module Option = Option
module Set = Set
module String = String
module Tuple2 = Tuple2
module Tezos = Tezos
module Next = Tezos.Next
module Operation = Tezos.Next.Operation
module Sapling = Tezos.Next.Sapling
module Ticket = Tezos.Next.Ticket
module View = Tezos.Next.View
module Test = Test

include Std.S

type nonrec string = String.string
type nonrec bytes = Bytes.bytes
type nonrec 'a list = 'a List.list = [] | (::) of 'a * 'a list
type ('key, 'value) big_map = ('key, 'value) Big_map.big_map
type ('key, 'value) map = ('key, 'value) Map.map
type 'value set = 'value Set.set

type ('param, 'storage) contract = ('param, 'storage) Std.contract = {
  address: address;
  balance: tez;
  delegate: key_hash option;
  storage: 'storage;
  entrypoint_name: string;
  entrypoint_fun: ('param -> 'storage -> (operation list * 'storage));
}

and operation = Std.operation = {
  sender: address;
  source: address;
  destination: address;
  entrypoint: string;
  amount: tez;
  param: string;
  operations: operation list;
}

type ('param, 'storage, 'res) view = ('param, 'storage, 'res) Std.view = {
  vstorage: 'storage;
  view: 'param -> 'storage -> 'res
}

type context = Std.context = {
  source: address;
  mutable sender: address;
  mutable self: address;
  level: int;
  time: timestamp;
  voting_power: nat;
  mutable amount: tez;
  chain_id: string;
  mutable contracts: (string, string) contract Stdlib.List.t;
  mutable views: ((address * string) * (string, string, string) view) list;
  constants: (string * (unit -> string)) list;
}

(* to use to build context *)
val mk_implicit_account : ?balance:nat -> ?delegate:key_hash -> address -> (string, string) contract
val mk_contract : ?balance:nat -> ?delegate:key_hash ->
  storage:'storage -> entrypoint:string -> address:address ->
  ('param -> 'storage -> operation list * 'storage) ->
  (string, string) contract
val mk_view : storage:'storage -> entrypoint:string -> address:address ->
  ('param -> 'storage -> 'res) -> ((string * string) * (string, string, string) view)
