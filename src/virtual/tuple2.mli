val curry : (('a * 'b) -> 'c) -> 'a -> 'b -> 'c
val uncurry : ('a -> 'b -> 'c) -> ('a * 'b) -> 'c
val get1 : ('a * 'b) -> 'a
val get2 : ('a * 'b) -> 'b
val swap : ('a * 'b) -> ('b * 'a)
