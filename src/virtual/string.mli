open Std
type nonrec string = string
val length : string -> nat
val sub : nat -> nat -> string -> string
val concat : string -> string -> string
val size : string -> nat
val concats : string list -> string
val slice : nat -> nat -> string -> string
