open Std

type bytes = string
val concat : bytes -> bytes -> bytes
val sub : nat -> nat -> bytes -> bytes
val pack : 'a -> bytes
val unpack : bytes -> 'a option
val length : bytes -> nat
val size : bytes -> nat
val concats : bytes list -> bytes
val slice : nat -> nat -> bytes -> bytes
