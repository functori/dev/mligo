open Std
type 'a list = 'a Stdlib.List.t = [] | (::) of 'a * 'a list
val length : 'a list -> nat
val size : 'a list -> nat
val head_opt : 'a list -> 'a option
val tail_opt : 'a list -> 'a list option
val iter : ('a -> unit) -> 'a list -> unit
val map : ('a -> 'b) -> 'a list -> 'b list
val fold : (('accumulator * 'item) -> 'accumulator) -> 'item list -> 'accumulator -> 'accumulator
val fold_left : (('accumulator * 'item) -> 'accumulator) -> 'accumulator -> 'item list -> 'accumulator
val fold_right : (('item * 'accumulator) -> 'accumulator) -> 'item list -> 'accumulator -> 'accumulator
val head : 'a list -> 'a option
val tail : 'a list -> 'a list option
val cons : 'a -> 'a list -> 'a list
val find_opt : ('a -> bool) -> 'a list -> 'a option
val filter_map : ('o -> 'n option) -> 'o list -> 'n list
val update : ('a -> 'a option) -> 'a list -> 'a list
val update_with : ('a -> bool) -> 'a -> 'a list -> 'a list
