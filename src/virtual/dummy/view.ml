open Std

let call ctx e arg addr =
  match ctx with
  | None -> None
  | Some ctx ->
    let old_sender = ctx.sender in
    let old_self = ctx.self in
    let old_amount = ctx.amount in
    ctx.sender <- old_self;
    ctx.self <- addr;
    ctx.amount <- 0;
    match Stdlib.List.assoc_opt (addr, e) ctx.views with
    | None -> None
    | Some v ->
      let r = Marshal.from_string (v.view (Marshal.to_string arg []) v.vstorage) 0 in
      ctx.sender <- old_sender;
      ctx.self <- old_self;
      ctx.amount <- old_amount;
      r
