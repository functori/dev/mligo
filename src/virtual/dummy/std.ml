module type S = sig
  type address = string
  type chain_id = string
  type key = string
  type key_hash = string
  type nat = int
  type signature = string
  type tez = int
  type timestamp = int
  type chest = unit
  type chest_key = string
  type never = unit

  type ('param, 'storage) contract = {
    address: address;
    balance: tez;
    delegate: key_hash option;
    storage: 'storage;
    entrypoint_name: string;
    entrypoint_fun: ('param -> 'storage -> (operation list * 'storage));
  }

  and operation = {
    sender: address;
    source: address;
    destination: address;
    entrypoint: string;
    amount: tez;
    param: string;
    operations: operation list }

  type bls12_381_fr = int
  type bls12_381_g1 = string
  type bls12_381_g2 = string
  type 'n sapling_state = 'n list
  type 'n sapling_transaction = 'n
  type 'v ticket = address * nat * 'v

  val is_nat : int -> nat option
  val abs : int -> nat
  val int : nat -> int
  val unit : unit
  val ediv : int -> int -> (int * nat) option

  val failwith : 'a -> 'b

  val nat : string -> nat
  val int_bytes : string -> int

  type ('param, 'storage, 'res) view = {
    vstorage: 'storage;
    view: 'param -> 'storage -> 'res
  }

  type context = {
    source: address;
    mutable sender: address;
    mutable self: address;
    level: int;
    time: timestamp;
    voting_power: nat;
    mutable amount: tez;
    chain_id: string;
    mutable contracts: (string, string) contract Stdlib.List.t;
    mutable views: ((address * string) * (string, string, string) view) list;
    constants: (string * (unit -> string)) list;
  }

end

type address = string
type chain_id = string
type key = string
type key_hash = string
type nonrec int = int
type nat = int
type signature = string
type tez = int
type timestamp = int
type chest = unit
type chest_key = string
type never = unit

type ('param, 'storage) contract = {
  address: address;
  balance: tez;
  delegate: key_hash option;
  storage: 'storage;
  entrypoint_name: string;
  entrypoint_fun: ('param -> 'storage -> (operation list * 'storage));
}

and operation = {
  sender: address;
  source: address;
  destination: address;
  entrypoint: string;
  amount: tez;
  param: string;
  operations: operation list}

type bls12_381_fr = int
type bls12_381_g1 = string
type bls12_381_g2 = string
type 'n sapling_state = 'n list
type 'n sapling_transaction = 'n
type 'v ticket = address * nat * 'v

let is_nat n = if n > 0 then Some n else None
let abs n = abs n
let int n = n
let unit = ()
let ediv n d =
  if d = 0 then None else Some (n / d, n mod d)

let failwith x =
  failwith (Marshal.to_string x [])

let nat b =
  int_of_string ("0x" ^ Hex.show (Hex.of_string b))
let int_bytes b =
  int_of_string ("0x" ^ Hex.show (Hex.of_string b))

type ('param, 'storage, 'res) view = {
    vstorage: 'storage;
    view: 'param -> 'storage -> 'res
  }

type context = {
  source: address;
  mutable sender: address;
  mutable self: address;
  level: int;
  time: timestamp;
  voting_power: nat;
  mutable amount: tez;
  chain_id: string;
  mutable contracts: (string, string) contract Stdlib.List.t;
  mutable views: ((address * string) * (string, string, string) view) list;
  constants: (string * (unit -> string)) list;
}
