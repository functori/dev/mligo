module Std = Std
module Big_map = Big_map
module Bitwise = Bitwise
module Bytes = Bytes
module Crypto = Crypto
module List = List
module Map = Map
module Option = Option
module Set = Set
module String = String
module Tuple2 = Tuple2
module Tezos = Tezos
module Next = Tezos.Next
module Operation = Tezos.Next.Operation
module Sapling = Tezos.Next.Sapling
module Ticket = Tezos.Next.Ticket
module View = Tezos.Next.View
module Test = Test

include Std

type nonrec string = String.string
type nonrec bytes = Bytes.bytes
type nonrec 'a list = 'a List.list = [] | (::) of 'a * 'a list
type ('key, 'value) big_map = ('key, 'value) Big_map.big_map
type ('key, 'value) map = ('key, 'value) Map.map
type 'value set = 'value Set.set

(* to use to build context *)
let mk_implicit_account ?(balance=0) ?delegate address = {
  address; balance; delegate; storage = Marshal.to_string () [];
  entrypoint_name = "%default"; entrypoint_fun = (fun _ s -> [], s)
}

let mk_contract ?(balance=0) ?delegate ~storage ~entrypoint ~address f = {
  address; balance; delegate; storage = Marshal.to_string storage [];
  entrypoint_name = "%" ^ entrypoint;
  entrypoint_fun = (fun p s ->
      let ops, s = f (Marshal.from_string p 0) (Marshal.from_string s 0) in
      ops, Marshal.to_string s [])
}

let mk_view ~storage ~entrypoint ~address f =
  (address, entrypoint),
  {vstorage=Marshal.to_string storage [];
   view = (fun p s ->
       let r = f (Marshal.from_string p 0) (Marshal.from_string s 0) in
       Marshal.to_string r [])}
