type 'a set = 'a Stdlib.List.t
let empty = []
let literal l = l
let mem x l = Stdlib.List.mem x l
let cardinal l = Stdlib.List.length l
let add x l = if mem x l then l else x :: l
let rec remove x = function
  | [] -> []
  | h :: q when h = x -> q
  | h :: q -> h :: remove x q
let update x b l =
  if b then add x l
  else remove x l
let iter f l = Stdlib.List.iter f l
let fold f l acc =
  Stdlib.List.fold_left (fun acc x -> f (acc, x)) acc l
let fold_desc f l acc =
  Stdlib.List.fold_right (fun x acc -> f (x, acc)) l acc
let of_list l = l
let size l = cardinal l
let filter_map f l = Stdlib.List.filter_map f l
let map f l = Stdlib.List.map f l
