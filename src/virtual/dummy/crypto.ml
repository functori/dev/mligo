let blake2b s = Hacl.blake2b s
let sha256 s = Hacl.sha256 s
let sha512 s = Hacl.sha512 s
let hash_key s = match Tzfunc.Crypto.pk_to_pkh s with
  | Ok s -> s
  | Error _ -> failwith "Not a edpk"
let check edpk edsig b =
  match Tzfunc.Crypto.Ed25519.verify ~edpk ~edsig ~bytes:(Tzfunc.Raw.mk b) with
  | Ok b -> b
  | Error _ -> failwith "Not a edpk or not a edsig"
let sha3 _s = failwith "sha3 not implementend"
let keccak _s = failwith "keccak not implementend"
