open Std

module Operation = Operation
module View = View
module Ticket = Ticket
module Sapling = Sapling

let get_sender = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.sender

let get_source = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.source

let self ctx e = match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    match Stdlib.List.find_opt (fun c ->
        c.address = ctx.self && c.entrypoint_name = e) ctx.contracts with
    | None -> failwith (Format.sprintf "No entrypoint named %S for %S in context" e ctx.self)
    | Some c ->
      { c with storage = Marshal.from_string c.storage 0;
               entrypoint_fun = (fun p s ->
                   let ops, s = c.entrypoint_fun (Marshal.to_string p []) (Marshal.to_string s []) in
                   ops, Marshal.from_string s 0) }

let get_self_address = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.self

let address c = c.address

let implicit_account ctx address =
  let c = { address; balance = 0; delegate = None; storage = (); entrypoint_name = "%default";
            entrypoint_fun = (fun () () -> [], ()) } in
  match ctx with
  | None -> c
  | Some ctx ->
    match Stdlib.List.find_opt (fun a -> a.address = address) ctx.contracts with
    | None -> c
    | Some a -> { c with balance = a.balance; delegate = a.delegate }

let get_entrypoint_opt ctx e addr = match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    Stdlib.List.find_map (fun a ->
        if a.address = addr && a.entrypoint_name = e then
          Some { a with
                 storage = Marshal.from_string a.storage 0;
                 entrypoint_fun = (fun p s ->
                     let ops, s = a.entrypoint_fun (Marshal.to_string p []) (Marshal.to_string s []) in
                     ops, Marshal.from_string s 0) }
        else None) ctx.contracts

let get_entrypoint ctx e addr = Stdlib.Option.get (get_entrypoint_opt ctx e addr)

let get_contract_opt ctx addr = get_entrypoint_opt ctx "%default" addr

let get_contract_with_error ctx addr err = match get_entrypoint_opt ctx "%default" addr with
  | None -> failwith err
  | Some c -> c

let get_contract ctx addr = Stdlib.Option.get @@ get_contract_opt ctx addr

let open_chest _ctx _k _chest _n = None

let get_balance = function
  | None -> failwith "no context given"
  | Some ctx ->
    match Stdlib.List.find_opt (fun c -> c.address = ctx.self) ctx.contracts with
    | None -> failwith "self not in context"
    | Some c -> c.balance

let get_amount = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.amount

let get_now = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.time

let get_min_block_time _ = 15

let get_level = function
  | None -> failwith "co context given"
  | Some ctx -> ctx.level

let get_chain_id = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.chain_id

let get_total_voting_power = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.voting_power

let voting_power ctx _ = match ctx with
  | None -> failwith "no context given"
  | Some ctx -> ctx.voting_power

let never _ = assert false

(* let pairing_check _ = false *)

let constant ctx s =
  match ctx with
  | None -> failwith "no context given"
  | Some ctx -> match Stdlib.List.assoc_opt s ctx.constants with
    | None -> failwith ( s ^ " not given in context constants" )
    | Some f -> Marshal.from_string (f ()) 0
