let curry f a b = f (a, b)
let uncurry f (a, b) = f a b
let get1 (x, _) = x
let get2 (_, x) = x
let swap (a, b) = b, a
