open Std

let create ctx v n = match ctx with
  | None -> None
  | Some ctx -> Some (ctx.sender, n, v)

let read _ t =
  let (addr, n, v) = t in
  (addr, (v, n)), t

let split _ t (n1, n2) =
  let (addr, n, v) = t in
  if n = n1 + n2 then Some ((addr, n1, v), (addr, n2, v))
  else None

let join _ (t1, t2) =
  let (addr1, n1, v1) = t1 in
  let (addr2, n2, _) = t2 in
  if addr1 = addr2 then Some (addr1, n1 + n2, v1)
  else None
