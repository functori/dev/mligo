open Std

let unopt x = Stdlib.Option.get x
let unopt_with_error x s = match x with
  | None -> failwith s
  | Some x -> x
let map f x = Stdlib.Option.map f x
let value x = unopt x
let value_with_error e = function
  | None -> raise e
  | Some x -> x
let is_none x = Stdlib.Option.is_none x
let is_some x = Stdlib.Option.is_some x
