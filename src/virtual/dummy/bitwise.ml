open Std

let and_nat a b = a land b
let or_nat a b = a lor b
let xor_nat a b = a lxor b
let shift_left_nat a b = a lsl b
let shift_right_nat a b = a lsr b

let bytes_of_int i =
  Hex.to_string (`Hex (Format.sprintf "%x" i))

let and_bytes a b = bytes_of_int @@ (nat a) land (nat b)
let or_bytes a b = bytes_of_int @@ (nat a) lor (nat b)
let xor_bytes a b = bytes_of_int @@ (nat a) lxor (nat b)
let shift_left_bytes a b = bytes_of_int @@ (nat a) lsl b
let shift_right_bytes a b = bytes_of_int @@ (nat a) lsr b
