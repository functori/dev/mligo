open Std

let create_contract ctx main delegate balance storage =
  match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    let address = Tzfunc.Crypto.(
        Base58.encode ~prefix:Prefix.contract_public_key_hash @@
        Raw.mk @@ Stdlib.String.init 20 (fun _ -> Char.chr @@ Random.int 256)) in
    ctx.contracts <- ctx.contracts @ [ {
        address; balance; delegate; storage = Marshal.to_string storage [];
        entrypoint_name = "default";
        entrypoint_fun = (fun p s ->
      let ops, s = main (Marshal.from_string p 0) (Marshal.from_string s 0) in
      ops, Marshal.to_string s [])
      } ];
    let operation = {
      sender = ctx.sender; source = ctx.source; amount = balance;
      destination = address;
      entrypoint = "origination"; param = "";
      operations = [] } in
    operation, address

let set_delegate ctx delegate = match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    let contracts = Stdlib.List.map (fun a ->
        if a.address = ctx.self then { a with delegate }
        else a) ctx.contracts in
    ctx.contracts <- contracts;
    let param = match delegate with None -> "None" | Some d -> d in
    { sender = ctx.self; source = ctx.source; amount=0; destination = ctx.self;
      entrypoint = "set_delegate"; param; operations = [] }

let transaction ctx param amount c =
  match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    let old_sender = ctx.sender in
    let old_self = ctx.self in
    let old_amount = ctx.amount in
    ctx.sender <- old_self;
    ctx.self <- c.address;
    ctx.amount <- amount;
    let operations, storage = c.entrypoint_fun param c.storage in
    ctx.sender <- old_sender;
    ctx.self <- old_self;
    ctx.amount <- old_amount;
    let contracts = Stdlib.List.map (fun a ->
        if a.address = c.address then
          { a with storage = Marshal.to_string storage []; balance = a.balance - amount }
        else a) ctx.contracts in
    ctx.contracts <- contracts;
    let views = Stdlib.List.map (fun ((a, e), v) ->
        if a = c.address then
          (a, e), {v with vstorage = Marshal.to_string storage []}
        else (a, e), v) ctx.views in
    ctx.views <- views;
    { sender = ctx.self; source = ctx.source; amount; destination = c.address;
      entrypoint = c.entrypoint_name; param = Marshal.to_string param [];
      operations }

let emit _ _ _ = failwith "not implemented"
