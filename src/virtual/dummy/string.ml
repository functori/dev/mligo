type nonrec string = string
let length = Stdlib.String.length
let sub i j s = Stdlib.String.sub s i j
let concat a b = a ^ b
let size s = length s
let concats l = Stdlib.String.concat "" l
let slice i j s = sub i j s
