type 'a list = 'a Stdlib.List.t = [] | (::) of 'a * 'a list
let length = Stdlib.List.length
let size = Stdlib.List.length
let head_opt l = try Some (Stdlib.List.hd l) with _ -> None
let tail_opt l = try Some (Stdlib.List.tl l) with _ -> None
let iter f l = Stdlib.List.iter f l
let map f l = Stdlib.List.map f l
let fold f l acc =
  Stdlib.List.fold_left (fun acc x -> f (acc, x)) acc l
let fold_left f acc l = Stdlib.List.fold_left (fun acc x -> f (acc, x)) acc l
let fold_right f l acc = Stdlib.List.fold_right (fun x acc -> f (x, acc)) l acc
let head l = head_opt l
let tail l = tail_opt l
let cons a l = Stdlib.List.cons a l
let find_opt f l = Stdlib.List.find_opt f l
let filter_map f l = Stdlib.List.filter_map f l
let update f l = Stdlib.List.map (fun x -> match f x with None -> x | Some x -> x) l
let update_with f y l = Stdlib.List.map (fun x -> if f x then y else x) l
