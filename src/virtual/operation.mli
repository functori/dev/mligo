open Std

val create_contract : context option ->
  ('parameter -> 'storage -> (operation list * 'storage)) ->
  key_hash option -> tez -> 'storage -> (operation * address)
val set_delegate : context option -> key_hash option -> operation
val transaction : context option -> 'parameter -> tez -> ('parameter, _) contract -> operation
val emit : context option -> string -> 'a -> operation
