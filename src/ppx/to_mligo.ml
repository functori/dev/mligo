open Ppxlib
open Ast_builder.Default

let dir = ref ""
let keep_prefix = ref false
let ocp_indent = ref false

let field_alias : (string, string) Hashtbl.t = Hashtbl.create 512

let rec transform_expr f expr =
  let loc = expr.pexp_loc in
  match expr.pexp_desc with
  | Pexp_constant (Pconst_integer (s, Some 't')) ->
    evar ~loc (s ^ "tez")
  | Pexp_constant (Pconst_integer (s, Some 'u')) ->
    evar ~loc (s ^ "mutez")
  | Pexp_constant (Pconst_integer (s, Some 'h')) ->
    evar ~loc s
  | Pexp_record (l, e) ->
    let l = List.map (function
        | (id1, ({pexp_desc = Pexp_ident id2; _} as e)) when id1.txt = id2.txt ->
          let id2 = {loc=id2.loc; txt = Longident.parse @@ (Longident.name id2.txt ^ " ")} in
          let id1_alias = match Hashtbl.find_opt field_alias (Longident.name id1.txt) with
            | None -> id1
            | Some alias -> { id1 with txt = Lident alias } in
          (id1_alias, {e with pexp_desc = Pexp_ident id2})
        | (id, e) ->
          let id_alias = match Hashtbl.find_opt field_alias (Longident.name id.txt) with
            | None -> id
            | Some alias -> { id with txt = Lident alias } in
          (id_alias, transform_expr f e)) l in
    {expr with pexp_desc = Pexp_record (l, Option.map (transform_expr f) e)}
  | Pexp_field (e, lid) ->
    let e = transform_expr f e in
    let lid_name = Longident.name lid.txt in
    let lid_alias = match Hashtbl.find_opt field_alias lid_name with
      | None -> lid_name
      | Some alias -> alias in
    let txt = Longident.parse @@
      (Pprintast.string_of_expression e) ^ "." ^ lid_alias in
    { e with pexp_desc = Pexp_ident {lid with txt} }
  | Pexp_ident {txt; loc} ->
    let id = Longident.name txt in
    let id =
      if id = "and_nat" || id = "and_bytes" then "and"
      else if id = "or_nat" || id = "or_bytes" then "or"
      else if id = "xor_nat" || id = "xor_bytes" then "xor"
      else if id = "shift_left_nat" || id = "shift_left_bytes" then "shift_left"
      else if id = "shift_right_nat" || id = "shift_right_bytes" then "shift_right"
      else if id = "int_bytes" then "int"
      else id in
    evar ~loc id
  | Pexp_apply ({pexp_desc=Pexp_ident {txt; loc}; _}, _h :: q) ->
    let txt = Longident.name txt in
    let aux0 s =
      let args = if q = [] then [ Nolabel, eunit ~loc ] else q in
      f (pexp_apply ~loc (evar ~loc ("Tezos.Next." ^ s)) args) in
    let rec aux = function
      | [] -> f expr
      | h :: t ->
        if txt = h || txt = "Tezos.Next." ^ h || txt = "Next." ^ h then aux0 h
        else aux t in
    aux [ "get_balance"; "get_now"; "get_amount"; "get_sender"; "get_self_address";
          "get_source"; "get_level"; "get_chain_id"; "get_total_voting_power";
          "self"; "voting_power"; "Operation.transaction"; "Operation.set_delegate"; "get_contract";
          "get_contract_opt"; "get_contract_with_error"; "get_entrypoint_opt"; "get_entrypoint";
          "implicit_account"; "open_chest"; "View.call"; "Operation.create_contract"; "constant";
          "Sapling.empty_state"; "Sapling.verify_update"; "Ticket.create";
          "Ticket.read"; "Ticket.split"; "Ticket.join";
          "get_min_block_time"; "Operation.emit" ]
  | Pexp_coerce (e, _, _) -> f e
  | Pexp_sequence ({pexp_desc=Pexp_extension ({txt="mligo"; _}, _); _}, e) -> f e
  | _ -> f expr

let rec transform_pat f p =
  let loc = p.ppat_loc in
  match p.ppat_desc with
  | Ppat_constant (Pconst_integer (s, Some 't')) ->
    pvar ~loc (s ^ "tez")
  | Ppat_constant (Pconst_integer (s, Some 'u')) ->
    pvar ~loc (s ^ "mutez")
  | Ppat_constant (Pconst_integer (s, Some 'h')) ->
    pvar ~loc s
  | Ppat_record (l, flag) ->
    let l = List.map (function
        | (id, p) ->
          let id_alias = match Hashtbl.find_opt field_alias (Longident.name id.txt) with
            | None -> id
            | Some alias -> { id with txt = Lident alias } in
          (id_alias, transform_pat f p)) l in
    {p with ppat_desc = Ppat_record (l, flag)}
  | Ppat_constraint (pat, ({ptyp_desc=Ptyp_constr (lid, [pa; _st]); _} as c)) ->
    let name = Longident.name lid.txt in
    if name = "contract" || name = "Tezos.contract" then
      f { p with ppat_desc = Ppat_constraint
                   (pat, {c with ptyp_desc = Ptyp_constr (lid, [pa]) }) }
    else f p
  | _ -> f p

let remove_prefix s n = String.sub s n (String.length s - n)

let same_prefix l =
  let common_prefix s1 s2 =
    let n1 = String.length s1 in
    let n2 = String.length s2 in
    let rec aux i =
      if i < n1 && i < n2 && s1.[i] = s2.[i] then aux (i+1)
      else i, String.sub s1 0 i in
    aux 0 in
  let rec aux n pr = function
    | [] -> n, pr
    | h :: t ->
      let n, pr = common_prefix h pr in
      aux n pr t in
  match l with
  | [] | [ _ ] -> 0
  | h :: t -> fst (aux (String.length h) h t)

let transform_type fpld fpcd fcore p =
  let ptype_attributes = List.filter (fun a -> a.attr_name.txt = "comb") p.ptype_attributes in
  match p.ptype_kind, p.ptype_manifest with
  | Ptype_record l, _ ->
    let l = List.map (fun pld ->
        let name = pld.pld_name.txt in
        let alias =
          List.fold_left (fun acc a -> match a.attr_name.txt, a.attr_payload with
              | "key", PStr [ {pstr_desc = Pstr_eval (
                  {pexp_desc = Pexp_constant (Pconst_string (s, _, _)); _}, _); _} ] ->
                Hashtbl.add field_alias name s;
                Some s
              | _ -> acc) None pld.pld_attributes in
        match alias with
        | None ->
          if !keep_prefix then
            fpld { pld with pld_attributes = [] } []
          else
            let n = same_prefix @@ List.map (fun pld -> pld.pld_name.txt) l in
            if n > 0 then
              let txt = remove_prefix name n in
              Hashtbl.add field_alias name txt;
              fpld { pld with pld_attributes = []; pld_name = {pld.pld_name with txt} } []
            else
              fpld { pld with pld_attributes = [] } []
        | Some txt -> fpld { pld with pld_attributes = []; pld_name = {pld.pld_name with txt} } []) l in
    let l = List.map fst l in
    { p with ptype_attributes; ptype_kind = Ptype_record l;
             ptype_private=Public}
  | Ptype_variant l, _ ->
    { p with ptype_attributes;
             ptype_kind = Ptype_variant (List.map (fun pcd -> fst (fpcd pcd [])) l);
             ptype_private=Public }
  | Ptype_abstract, Some m ->
    { p with ptype_attributes;
             ptype_kind = Ptype_abstract;
             ptype_manifest = Some (fst (fcore m []));
             ptype_private=Public }
  | _ ->
    {p with ptype_attributes; ptype_private=Public}

let value_to_string ?(first=false) ?(r_flag=Nonrecursive) pat expr =
  let le =
    if first && r_flag = Recursive then "let rec"
    else if first then "let"
    else if r_flag = Recursive then "and"
    else "let" in
  let pat, cons0 = match pat.ppat_desc with
    | Ppat_constraint (p, c) -> p, Some c
    | _ -> pat, None in
  Pprintast.pattern Format.str_formatter pat;
  let name = Format.flush_str_formatter () in
  let rec aux acc expr = match expr.pexp_desc with
    | Pexp_fun (_, _, p, e) -> aux (p :: acc) e
    | Pexp_constraint (e, c) -> acc, e, Some c
    | _ -> acc, expr, None in
  let args, expr, cons = aux [] expr in
  let expr = Pprintast.string_of_expression expr in
  let cons = match cons, cons0 with
    | None, None -> ""
    | Some c, _  | _, Some c ->
      Pprintast.core_type Format.str_formatter c;
      " : " ^ Format.flush_str_formatter () in
  let args = String.concat " " @@ List.map (fun p ->
      Pprintast.pattern Format.str_formatter p;
      Format.flush_str_formatter ()) (List.rev args) in
  Format.sprintf "%s %s %s%s =\n%s" le name args cons expr

let rec replace_extension ~name ?(before="") ?(after="") ?inc s =
  let n = String.length s in
  let n_name = String.length name in
  let rec get_end acc i =
    if i >= n then failwith "no end to extension"
    else
      let c = String.get s i in
      if c = '[' then get_end (acc+1) (i+1)
      else if c = ']' then
        let acc = acc - 1 in
        if acc = 0 then i
        else get_end acc (i+1)
      else get_end acc (i+1) in
  let rec aux i =
    if i+n_name+3 >= n then s
    else
      let x = String.sub s i (n_name+3) in
      if x = "[%%" ^ name then
        let j = get_end 1 (i+n_name+3) in
        let next = replace_extension ~name ~before ~after (String.sub s (j+1) (n-j-1)) in
        let inside = String.trim (String.sub s (i+n_name+3) (j-i-n_name-3)) in
        let inside = match inc with
          | None -> inside
          | Some (k, x) -> match String.index_opt inside k with
            | None -> inside
            | Some m -> String.sub inside 0 (m+1) ^ x ^ String.sub inside (m+1) (String.length inside - m - 1) in
        String.sub s 0 i ^ before ^ inside ^ after ^ next
      else aux (i+1) in
  aux 0

let rec replace_includes s =
  let n = String.length s in
  let replace s = match String.index_opt s '[' with
    | None -> Format.sprintf "#include \"./%s.mligo\"" (String.uncapitalize_ascii s)
    | Some i ->
      let module_ = String.trim (String.sub s 0 i) in
      if i+7 < n && String.sub s i 7 = "[@@path" then
        let end_ = String.index_from s (i+7) ']' in
        let path = String.trim @@ String.sub s (i+7) (end_-i-7) in
        Format.sprintf "#include %s" path
      else
        Format.sprintf "#include \"./%s.mligo\"" (String.uncapitalize_ascii module_) in
  let rec aux i =
    if i+8 >= n then s
    else if String.sub s i 8 = "include " then
      match String.index_from_opt s (i+8) '\n' with
      | Some j -> String.sub s 0 i ^ replace (String.sub s (i+8) (j-i-8)) ^ replace_includes (String.sub s (j+1) (n-j-1))
      | None -> String.sub s 0 i ^ replace (String.sub s (i+8) (n-i-8))
    else aux (i+1) in
  aux 0

let rec replace_modules s =
  let n = String.length s in
  let get_import m x =
    let path = match String.index_opt s '[' with
      | None -> "\"./" ^ (String.uncapitalize_ascii @@ String.trim x) ^ ".mligo\""
      | Some i ->
        if i+7 < n && String.sub s i 7 = "[@@path" then
          let end_ = String.index_from s (i+7) ']' in
          String.trim @@ String.sub s (i+7) (end_-i-7)
        else
          "\"./" ^ (String.uncapitalize_ascii @@ String.trim x) ^ ".mligo\"" in
    Format.sprintf "#import %s %S\n" path (String.trim m) in
  let rec aux i =
    if i+7 >= n then s
    else if String.sub s i 7 = "module " then
      match String.index_from_opt s (i+7) '\n' with
      | Some j ->
        begin match String.split_on_char '=' @@ String.sub s (i+7) (j-i-7) with
          | [ m1; m2 ] when String.trim m2 <> "struct" ->
            String.sub s 0 i ^ get_import m1 m2 ^ replace_modules (String.sub s (j+1) (n-j-1))
          | _ -> String.sub s 0 j ^ replace_modules (String.sub s j (n-j))
        end
      | None ->
        begin match String.split_on_char '=' @@ String.sub s (i+7) (n-i-7) with
          | [ m1; m2 ] when String.trim m2 <> "struct" ->
            String.sub s 0 i ^ get_import m1 m2
          | _ -> s
        end
    else aux (i+1) in
  aux 0

let rec transform () =
  object(_self)
    inherit [string list] Ast_traverse.fold_map as super

    method! expression e _ =
      transform_expr (fun e -> fst @@ super#expression e []) e, []

    method! pattern p _ =
      transform_pat (fun p -> fst @@ super#pattern p []) p, []

    method! core_type c _ =
      let loc = c.ptyp_loc in
      match c.ptyp_desc with
      | Ptyp_constr (lid, [pa; _st]) ->
        let name = Longident.name lid.txt in
        if name = "contract" || name = "Tezos.contract" then
          super#core_type {c with ptyp_desc = Ptyp_constr (lid, [pa]) } []
        else super#core_type c []
      | Ptyp_var s ->
        super#core_type
          { c with ptyp_desc = Ptyp_constr ({txt=Lident ("_" ^ s); loc}, []) } []
      | _ ->
        super#core_type c []

    method! constructor_declaration p acc =
      super#constructor_declaration {p with pcd_attributes = []} acc

    method! structure l _ =
      let rec aux l =
        List.fold_left (fun acc st ->
            match st.pstr_desc with
            | Pstr_open _ -> acc
            | Pstr_include {pincl_mod = {pmod_desc = Pmod_ident {txt = Lident id; _}; _}; pincl_attributes; _} ->
              let name, no_write = match pincl_attributes with
                | [ {attr_name={txt="path"; _};
                     attr_payload = PStr [ {pstr_desc=Pstr_eval ({
                       pexp_desc=Pexp_constant Pconst_string (s, _, _); _}, _); _} ]; _} ] ->
                    Filename.remove_extension s, true
                | _ -> String.uncapitalize_ascii id, false in
              process ~no_write name;
              acc @ [ Pprintast.string_of_structure [st] ]
            | Pstr_module {pmb_name = {txt = Some _; _};
                           pmb_expr = {pmod_desc = Pmod_ident {txt=Lident id; _}; _}; pmb_attributes; _} ->
              let name, no_write = match pmb_attributes with
                | [ {attr_name={txt="path"; _};
                     attr_payload = PStr [ {pstr_desc=Pstr_eval ({
                       pexp_desc=Pexp_constant Pconst_string (s, _, _); _}, _); _} ]; _} ] ->
                    Filename.remove_extension s, true
                | _ -> String.uncapitalize_ascii id, false in
              process ~no_write name;
              acc @ [ Pprintast.string_of_structure [st] ]
            | Pstr_module ({pmb_expr = {pmod_desc = Pmod_constraint (m, _); _}; _} as pmb) ->
              let pmb_expr, _ = super#module_expr m [] in
              acc @ [ Pprintast.string_of_structure [
                  { st with pstr_desc = Pstr_module {pmb with pmb_expr} } ] ]
            | Pstr_value (r_flag, pvl) ->
              let l, _ = List.fold_left (fun (acc, first) pv ->
                  if not (List.exists (fun a -> a.attr_name.txt = "ignore") pv.pvb_attributes) then
                    let pv, _ = super#value_binding pv [] in
                    acc @ [value_to_string ~first ~r_flag pv.pvb_pat pv.pvb_expr], false
                  else
                    acc, first) ([], true) pvl in
              if l = [] then acc
              else acc @ l
            | Pstr_type (r_flag, l) ->
              let l = List.filter_map (fun t ->
                  if not (List.exists (fun a -> a.attr_name.txt = "ignore") t.ptype_attributes) then
                    Some (transform_type super#label_declaration super#constructor_declaration super#core_type t)
                  else
                    None) l in
              if l = [] then acc
              else (
                let s2 = Pprintast.string_of_structure [{st with pstr_desc = Pstr_type (r_flag, l)}] in
                acc @ [ s2 ])
            | Pstr_modtype _ -> acc
            | Pstr_extension (({txt; _}, PStr s) , _attrs) ->
              let acc2 = aux s in
              if acc2 = [] then acc
              else acc @ [ Format.sprintf "[%%%%%s\n%s]" txt (String.concat "\n\n" acc2) ]
            | _ ->
              acc @ [ Pprintast.string_of_structure @@ [fst @@ super#structure_item st [] ] ]) [] l in
      l, aux l
  end

and process ?(no_write=false) name =
  let path = Filename.concat !dir name in
  if no_write then
    try
      let ic = open_in (path ^ ".ml") in
      let s = really_input_string ic (in_channel_length ic) in
      close_in ic;
      let lexbuf = Lexing.from_string s in
      let str = Parse.implementation lexbuf in
      ignore @@ (transform ())#structure str []
    with _ -> ()
  else
    let ic = open_in (path ^ ".ml") in
    let s = really_input_string ic (in_channel_length ic) in
    close_in ic;
    let lexbuf = Lexing.from_string s in
    let str = Parse.implementation lexbuf in
    let _, l = (transform ())#structure str [] in
    let s = String.concat "\n\n" l in
    let tmp_file = Filename.temp_file (Filename.basename name) "mligo" in
    let oc = open_out tmp_file in
    output_string oc s;
    close_out oc;
    if !ocp_indent then
      ignore @@ Sys.command @@ Filename.quote_command ~stdout:(path ^ ".mligo") "ocp-indent" [ tmp_file ]
    else
      ignore @@ Sys.command (Format.sprintf "cp %s %s.mligo" tmp_file path);
    let ic = open_in (path ^ ".mligo") in
    let s = really_input_string ic (in_channel_length ic) in
    close_in ic;
    let s = replace_extension ~name:"inline" ~before:"[@inline]\n" s in
    let s = replace_extension ~name:"entry" ~before:"[@entry]\n" s in
    let s = replace_extension ~name:"view" ~before:"[@view]\n" s in
    let s = replace_extension ~name:"tree" ~inc:('=', " [@layout:tree]") s in
    let s = replace_extension ~name:"comb" ~inc:('=', " [@layout:comb]") s in
    let s = replace_extension ~name:"define" ~before:"#define " s in
    let s = replace_extension ~name:"else" ~before:"#else\n" ~after:"\n#endif" s in
    let s = replace_extension ~name:"if_" ~before:"#if " s in
    let s = replace_extension ~name:"elif_" ~before:"#elif " s in
    let s = replace_includes s in
    let s = replace_modules s in
    let oc = open_out (path ^ ".mligo") in
    output_string oc s;
    close_out oc

let () =
  let filename = ref None in
  let stdout = Filename.temp_file "to_mligo_ocp_indent" "" in
  ocp_indent := (
    match Sys.command @@ Filename.quote_command ~stdout "ocp-indent" ["--version"] with
      | 0 -> true
      | _ -> false);
  Arg.parse ["--keep-prefix", Arg.Set keep_prefix, "keep record prefix"  ] (fun f -> filename := Some f) "";
  match !filename with
  | None -> ()
  | Some f ->
    dir := Filename.dirname f;
    process @@ Filename.basename @@ Filename.remove_extension f
