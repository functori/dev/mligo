let ligo_prefix = match Sys.getenv_opt "LIGO_PREFIX" with
  | None -> ref "/usr/local/bin"
  | Some s -> ref s

let static_links = [
  "dev", "https://ligolang.org/bin/linux/ligo";
  "1.9.2", "https://gitlab.com/ligolang/ligo/-/jobs/8861513110/artifacts/raw/ligo";
  "1.8.1", "https://gitlab.com/ligolang/ligo/-/jobs/8618620191/artifacts/raw/ligo";
  "1.6.0", "https://gitlab.com/ligolang/ligo/-/jobs/6629577754/artifacts/raw/ligo";
  "1.5.0", "https://gitlab.com/ligolang/ligo/-/jobs/6506566842/artifacts/raw/ligo";
  "1.4.0", "https://gitlab.com/ligolang/ligo/-/jobs/6303579494/artifacts/raw/ligo";
  "1.3.0", "https://gitlab.com/ligolang/ligo/-/jobs/6056572676/artifacts/raw/ligo";
  "1.2.0", "https://gitlab.com/ligolang/ligo/-/jobs/5696744559/artifacts/raw/ligo";
  "1.1.0", "https://gitlab.com/ligolang/ligo/-/jobs/5419828800/artifacts/raw/ligo";
  "1.0.0", "https://gitlab.com/ligolang/ligo/-/jobs/5178682732/artifacts/raw/ligo";
]

let version = ref (fst @@ List.nth static_links 1)
let docker = ref false
let reset = ref false

let installed () =
  let stdout = Filename.temp_file "ligo_install_version" "" in
  Sys.command @@ Filename.quote_command ~stdout "ligo" ["--version"] = 0

let static_install () =
  let link = Option.value ~default:"https://ligolang.org/bin/linux/ligo" @@
    List.assoc_opt !version static_links in
  let r = Sys.command @@ Format.sprintf
      "curl -s -o ligo %s && \
       chmod +x ligo && mv ligo %s" link !ligo_prefix in
  if r <> 0 then Format.printf "Ligo install failed@."

let docker_install () =
  let oc = open_out_gen [ Open_wronly; Open_text; Open_creat; Open_trunc ] 0o555 @@
    Filename.concat !ligo_prefix "ligo" in
  output_string oc
    (Format.sprintf "#!/bin/sh\ndocker run --rm -v $PWD:$PWD -w $PWD ligolang/ligo:%s $@" !version);
  close_out oc

let spec = [
  "--version", Arg.Set_string version, "version to install";
  "--docker", Arg.Set docker, "install docker version";
  "--reset", Arg.Set reset, "force reinstallation";
  "--prefix", Arg.Set_string ligo_prefix, "installation directory (default: /usr/local/bin)";
]

let main () =
  let ic = Unix.open_process_in "uname" in
  let uname = String.trim @@ input_line ic in
  close_in ic;
  if uname = "Linux" && not !docker then static_install ()
  else docker_install ()

let () =
  Arg.parse spec (fun _ -> ()) "ligo_install";
  if not !reset && installed () then ()
  else if !reset then
    let _ = Sys.command @@ Format.sprintf "rm -f %s/ligo" !ligo_prefix in
    main ()
  else main ()
